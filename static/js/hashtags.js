// Highlight hashtags in notes.
$(document).ready(function() {
        $('.note').each(function(i, obj) {
        var text = obj.innerHTML;
        var exp = /\#([a-zA-Z0-9_-]*)/gim;
        obj.innerHTML = text.replace(exp, function(match) {
            var match = match.substr(1);
            return `<a href="/tags/${match.toLowerCase()}">#${match}</a>`;
        });
    })
})
